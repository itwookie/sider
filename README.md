# SiDeR
<b>[Si]</b>mple <b>[De]</b>pendency <b>[R]</b>esolution

SiDeR is a tool, intended primarily for the use on windows systems, as a 
fire-and-forget setup utility that prepares and compiles desired dependencies.
The idea came to mind when I struggled using c++ dependencies on Windows, 
which is (compared to unix) a real pain.  
The goal is to ultimately provide and evironment that presents a hyper-local 
installation of all neccessary tools and dependencies required to work on your
project. This means you do not have to install additional build tools, not 
will those be visibly installed (portable installs wherever possible). All 
headers and library files for your project get collected into shared directories
just like on linux.  
This is in no way meant to replace other build tools, but to make things easier 
for people, that did not work with c/c++ dependencies before.

### How does this work?

SiDeR will automatically fetch neccessary build tools, download dependencies,
make them and redirect `/include`s and `/libs` into a shared directory that
can be used very similar to `/usr/include` and `/usr/libs` on unix.

In order for this to happen you can write a `.sdr` script or use the SiDeR-
shell.  
Tools go into `/tools/` and require a install script being present,  
Dependencies go into `/modules/` and will always download.

While tools have to generate a tool descriptor in 
`/tools/<TOOLDIR>/tools.sider` to automatically provide environment variables 
and tool paths, dependencies require a module descriptor at 
`/module/<AUTHOR>/<NAME>/module.sider`.

If a dependency does not ship with a module descriptor a basic one will 
automatically be created.

Idealy there'd be a repository for projects to automatically fetch missing 
dependencies from, but that's currently not the case.

### .sdr scripts

This is a simple language wrapping certain functions.  
Lines that start with `#`, `//` or `'` are comments.

There are two syntax elements: tokens and the Rest-of-Line. A token is the next
word or quoted string (quotes get removed) and the Rest-of-Line is anything 
from the last token to the end of line.  
Tokens that denote or represent a path support a `{{RegEx}}` plapceholder that
get's replaces by the first file or folder matching the regex as this part ot
the path. Note that the regex is required to fill one path element, so 
expressions like `folder/sub{{.*}}/` are not allowed. A valid example would be
`folder/{{sub.*}}/`.

There are conditional prefixes, that allow to limit actions to certain platforms:

| Line prefix | Result |
| ----------- | ------ |
| `@64` | Only execute on x64 environments |
| `@32` | Only execute on x86 environments |
| `@Win` | Only execute on Windows |
| `@Unix` | Only execute on Linux/Unix |
| `@Mac` | Only execute on Mac* <br><sup> *Mac in not fully supported </sup> |


In the following list `<UPPERCASE>` denote required tokens, `[UPPERCASE]` 
denotes an optional token and `{UPPERCASE}` denotes a Rest-of-Line

#### Utility

**`return`**  
halt script execution at this line and return

**`tool <LABEL> of <SCRIPT>`**  
Defines a tool dependency similar to #include.  
If no tool is installed, that provides a tool named LABEL the `.sdr`-script 
`/tools/<SCRIPT>.sdr` will be executed to install said tool.

**`get <LABEL> <URL> [QUERYSELECTOR]`**  
Downloads a file or archive into `/downloads/<LABEL>/`.  
If QUERYSELECTOR is specified URL is opened as website and an &lt;a&gt;-html 
element matching the js QUERYSELECTOR will be used as URL provider.  
If URL ends with .git it will be treated as GIT-URL and be cloned into 
`/downlaods/<REPO-NAME>/`

**`install <AUTHOR> <NAME> <VERSION> from <SOURCE>`**  
Unpacks a module version from SOURCE into `modules/AUTHOR/NAME/`  
SOURCE can be a archive file with one of these extensions: `.zip`, `.gz`, 
`.gzip`, `.bz2`, `.bzip2`, `.tar`, `.rar`, `.7z` or the link to a `.git` file, 
where the version will be used to download a TAG with the same name.  
After extraction the module directory is expected to hold a `module.sider` that
further describes the module. If non is found a template `module.sider` will be 
created.   
In case the `module.sider` specifies a setup script, that script will be 
executed in a new ScriptEngine instance to perform additional installation 
steps.   
After installing a indexing cycle will be invoked, copying module includes and 
libs into the shared directories `include/` and `libs/`.

**`build <AUTHOR> <NAME>`**   
Can be used to manually invoke the setup script specified in the `module.sider` 
after the module has been installed.   
After executing the setup script an indexing cycle will be invoked, copying
module includes and libs into the shared directories `include/` and `libs/`.

**`clean`**   
Deletes all files from the shared directories `include/` and `libs/` before
invoking an indexing cycle.

**`search [AUTHOR] <NAME>)`**  
Searches and lists installed modules and tools. NAME or AUTHOR are regular 
expressions that are used to filter modules and tools (only one argument). The
list of modules and tools will be printed including the installed version.

**`.<LABEL> {ARGUMENTS}`**  
Executes the tool called LABEL with the specified ARGUMENTS.  
Tools are defined by tool descriptors in a `/tools/<TOOL>`-directory.

**`run <COMMAND>`**  
Runs a executable in the context of SiDeR. CWD is preserved.

**`syscall <COMMAND>`**  
Runs the command through `cmd /c <COMMAND>` on windows or `bash -c <COMMAND>`
on unix. This is needed in case you want to run things like MOVE that are no 
executables.

#### File System

**`move <SOURCE> <DESTINATION>`**  
Moves a file or folder from SOURCE to DESTINATION.  
If SOURCE is a file and DESTINATION does *not* end with the Path-Separator 
(`/` or `\`) DESTINATION will be treated as file, otherwise it will be assmed 
that DESTINATION denotes a directory.

**`erase <ELEMENT>`**  
Remove ELEMENT from the filesystem.  
If ELEMENT is a directory all contents including ELEMENT will be deleted.

**`fopen <FILE>`**  
Opens a file to be written to.  
All following lines in the `.sdr`-script will be echoed into the FILE until 
the script runs into the line `fclose`.

**`fprintln <FILE> {LINE}`**
Appends LINE to FILE.  
*Note:* The file will not be closed by SiDeR until the script ends or `fclose`
is called. Multiple files can be opened at the same time.

**`fclose`**  
Close all currently open files.

**`mkdirs <PATH>`**   
Creates the directory structure specified with PATH, path elements are always 
treated as directory names. This PATH does **not** support RegEx

**`cwd <PATH>`**   
Change the working directory. The PATH is always relative to the current
work directory but supports RegEx elements (see above).

**`rename <SRC> <DEST>`**   
Meant to mass rename files in the cwd, if src is a RegEx numeric capture-groups 
are available in dest with `{{$X}}` where X is the index of the capture-group.
The list of files is cached before renaming in order to prevent recursion.

### tool.sider
Tool descriptors support the following commands

**`tool <LABEL> <RELPATH>`**  
define a tool to be used in `.sdr`-scripts. The RELPATH denotes the path to the
executable binary within TOOLDIR

**`env <set|add|addpath> <KEY> <VALUE>`**  
maipulates the virual environment variables used in `.sdr`-scripts.   
`set` replaces whatever KEY held with VALUE   
`add` appends VALUE to the current KEY   
`addpath` treats the KEY as path list and appends VALUE as relative path within 
TOOLDIR to the list

**`include <DIRECTORY>`**  
Specifies the directory relative to the tool directory, that shall be copied into
the shared include directory.

**`libs <DIRECTORY>`**  
Specifies the directory relative to the tool directory, that shall be copied into
the shared library directory.

**`bin <DIRECTORY>`**  
Specifies the directory relative to the tool directory, that shall be copied into
the shared binary directory.

### module.sider

**`module <AUTHOR> <NAME> <VERSION>`**  
Each module definition should start with this line, declaring the modules version. 
This information will be used for dependency resolution.

**`env <set|add|addpath> <KEY> <VALUE>`**  
maipulates the virual environment variables used in `.sdr`-scripts.   
`set` replaces whatever KEY held with VALUE   
`add` appends VALUE to the current KEY   
`addpath` treats the KEY as path list and appends VALUE as relative path within 
the modules directory to the list

**`include <DIRECTORY>`**  
Specifies the directory relative to the module directory, that shall be copied into
the shared include directory.

**`libs <DIRECTORY>`**  
Specifies the directory relative to the module directory, that shall be copied into
the shared library directory.

**`bin <DIRECTORY>`**  
Specifies the directory relative to the module directory, that shall be copied into
the shared binary directory.

**`depends <AUTHOR> <NAME> <VERSION-SPEC> as <LABEL>`**  
This adds a dependency with a given version specification to the module.  
Version specifications follow the [Maven Specification](https://maven.apache.org/enforcer/enforcer-rules/versionRanges.html).  
Requires `module` to be called prior.

**`deprule <RULE>`**
Specifies relations of dependencies and is required after all `depends` calls. Only
the last specified `deprule` takes effect.  
The RULE is a (spaceless) list of LABELs defined by `depends`, cancatinated with `&` 
(and) or `|` (or). It represents a OR-list of AND-dependencies, meaning if one 
AND-concatinated group of dependencies in the ORed-list of dependencies is present, 
the while dependency if fulfilled.

**`setup <SCRIPT>`**  
Specify a script that will be executed after `install`ing or when `build`ing the 
module.

### License Stuff

**The distribution and default tool scripts use 7zip to process archives.**  
7zip is licensed under the [GNU LGPL license](https://www.7-zip.org/license.txt) and available at https://www.7-zip.org   
**NOTE**: 7zip binaries are not included in any release, you'll have to drop `7z.exe` and `7z.dll` into `tools/7zip/`

**HTML parsing is done with JSoup**  
JSoup is licensed under the [MIT license](https://jsoup.org/license) and available at https://jsoup.org/  
Copyright © 2009 - 2017 Jonathan Hedley (jonathan@hedley.net)

**This Project itself**  
SiDeR is using the MIT license.  
Copyright © 2019 ITWookie (michael-kiermaier@gmx.de)

**The MIT license**  
Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.