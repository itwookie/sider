package com.itwookie.sider.cache;

import com.itwookie.sider.SiderUtil;

import java.io.*;
import java.util.*;
import java.util.stream.Collectors;

public class ModuleCrawler {

    private Set<Module> installed;
    public ModuleCrawler() {
        installed = new HashSet<>();
    }

    private static final char[] progress = new char[]{ '/', '-', '\\', '|' };
    private static int progressi = 0;
    private static void printProgress(String element) { System.out.printf( "%c %s%s\r", progress[(progressi=(++progressi)%progress.length)], element, new String(new char[79-(element.length()+2)]).replace('\0', ' ') ); }

    public void update() {
        installed.clear();
        File base = new File("module");
        if (!base.exists() || !base.isDirectory())
            if (!base.mkdirs()) return;
        for (File f : base.listFiles()) {
            if (f.isDirectory()) {
                for (File md : f.listFiles()) {
                    if (md.isDirectory()) {
                        File mi = new File(md, "module.sider");
                        if (mi.exists()) {
                            printProgress("Indexing module "+f.getName()+"-"+md.getName());
                            try {
                                installed.add(parseModuleDefinition(mi));
                            } catch (IOException e) {
                                System.err.println("Failed to load "+f.getName()+"-"+md.getName());
                                e.printStackTrace();
                            }
                        }
                    }
                }
            }
        }
        //TODO rework maybe?
        Set<String> missing = new HashSet<>();
        if (SiderUtil.VERBOSE.get())
            System.out.println(); //calling method writes something like "indexing... \r" - don't overwrite that line
        for (Module module : installed) {
            if (SiderUtil.VERBOSE.get())
                System.out.println("Checking Deps for "+module.getAuthor()+"-"+module.getName());
            if (!module.getDependencies().satisfied(installed)) {
                missing.add(module.getAuthor()+"-"+module.getName());
                //For automation: select the orSet in the DepRule that has the least missing dependencies and install
            }
        }
        if (!missing.isEmpty())
            throw new RuntimeException("Some modules are missing dependencies: "+String.join("; ", missing));
    }

    public static Module parseModuleDefinition(File moduleDefinition) throws IOException {
        BufferedReader br = null;
        File moduleDirectory = moduleDefinition.getParentFile();
        String line;
        Module thisModule = null;
        Map<String, Dependency> preDep = new HashMap<>();
        DepRule dependencies=null;
        try {
            br = new BufferedReader(new InputStreamReader(new FileInputStream(moduleDefinition)));
            while ((line = br.readLine()) != null) {
                if (line.isEmpty() || line.startsWith("#") || line.startsWith("'") || line.startsWith("//"))
                    continue;

                List<String> tokens = SiderUtil.tokenize(line);
                if (tokens.get(0).equalsIgnoreCase("module")) { // author name version
                    if (thisModule != null)
                        throw new RuntimeException("Module Definition file wants to register multiple modules");
                    if (tokens.size() < 4)
                        throw new RuntimeException("Incomplete or invalid module data in Module Difinition");
                    thisModule = new Module(tokens.get(1), tokens.get(2), tokens.get(3));
                } else if (tokens.get(0).equalsIgnoreCase("env")) {
                    if (tokens.get(1).equalsIgnoreCase("set")) {
                        SiderUtil.environmentSet(tokens.get(2), tokens.get(3));
                    } else if (tokens.get(1).equalsIgnoreCase("add")) {
                        SiderUtil.environmentAdd(tokens.get(2), tokens.get(3));
                    } else if (tokens.get(1).equalsIgnoreCase("addpath")) {
                        SiderUtil.environmentAddPath(tokens.get(2), new File(moduleDirectory, tokens.get(3)).getPath());
                    } else
                        throw new IOException("Invalid env transformer");

                } else if (tokens.get(0).equalsIgnoreCase("includes")) {
                    File src = new File(moduleDirectory, tokens.get(1));
                    if (!src.exists() || !src.isDirectory()) {
                        if (thisModule == null)
                            System.err.println("/!\\ include dir " + tokens.get(1) + " is missing");
                        else
                            System.err.println("/!\\ include dir " + tokens.get(1) + " from module " + thisModule.getAuthor()+"-"+thisModule.getName() + " is missing");
                        continue;
                    }
                    File dest = new File("include");
                    if (tokens.size() > 2) {
                        if (!tokens.get(2).equalsIgnoreCase("to"))
                            throw new IOException("Token 'TO' expected");
                        if (tokens.size() < 4)
                            throw new IOException("Missing include target directory");
                        dest = new File(dest, tokens.get(3));
                    }
                    if (!dest.exists() && !dest.mkdirs())
                        throw new IOException("Could not create shared include directory");
                    SiderUtil.copy_directory(src, dest, true);
                } else if (tokens.get(0).equalsIgnoreCase("libs")) {
                    File src = new File(moduleDirectory, tokens.get(1));
                    if (!src.exists() || !src.isDirectory()) {
                        if (thisModule == null)
                            System.err.println("/!\\ libs dir " + tokens.get(1) + " is missing");
                        else
                            System.err.println("/!\\ libs dir " + tokens.get(1) + " from module " + thisModule.getAuthor()+"-"+thisModule.getName() + " is missing");
                        continue;
                    }
                    File dest = new File("libs");
                    if (tokens.size() > 2) {
                        if (!tokens.get(2).equalsIgnoreCase("to"))
                            throw new IOException("Token 'TO' expected");
                        if (tokens.size() < 4)
                            throw new IOException("Missing libs target directory");
                        dest = new File(dest, tokens.get(3));
                    }
                    if (!dest.exists() && !dest.mkdirs())
                        throw new IOException("Could not create shared include directory");
                    SiderUtil.copy_directory(src, dest, true);
                } else if (tokens.get(0).equalsIgnoreCase("bin")) {
                    File src = new File(moduleDirectory, tokens.get(1));
                    if (!src.exists() || !src.isDirectory()) {
                        if (thisModule == null)
                            System.err.println("/!\\ bin dir " + tokens.get(1) + " is missing");
                        else
                            System.err.println("/!\\ bin dir " + tokens.get(1) + " from module " + thisModule.getAuthor()+"-"+thisModule.getName() + " is missing");
                        continue;
                    }
                    File dest = new File("bin");
                    if (tokens.size() > 2) {
                        if (!tokens.get(2).equalsIgnoreCase("to"))
                            throw new IOException("Token 'TO' expected");
                        if (tokens.size() < 4)
                            throw new IOException("Missing bin target directory");
                        dest = new File(dest, tokens.get(3));
                    }
                    if (!dest.exists() && !dest.mkdirs())
                        throw new IOException("Could not create shared include directory");
                    SiderUtil.copy_directory(src, dest, true);
                } else if (tokens.get(0).equalsIgnoreCase("depends")) {
                    if (thisModule == null)
                        throw new RuntimeException("DEPENDS expected after MODULE");
                    if (dependencies != null)
                        throw new RuntimeException("DEPENDS expected before DEPRULE");
                    Dependency dep = new Dependency(tokens.get(1), tokens.get(2), tokens.get(3));
                    tokens = tokens.subList(4, tokens.size());
                    if (!tokens.get(0).equalsIgnoreCase("as"))
                        throw new IOException("Token 'AS' expected");
                    preDep.put(tokens.get(1).toLowerCase(), dep);
                    if (tokens.size() > 2) {
                        if (!tokens.get(2).equalsIgnoreCase("from"))
                            throw new IOException("Token 'FROM' expected");
                        dep.setSearchSource(tokens.get(3));
                    }
                } else if (tokens.get(0).equalsIgnoreCase("deprule")) {
                    if (thisModule == null)
                        throw new RuntimeException("DEPRULE expected after MODULE");
                    if (preDep.isEmpty())
                        throw new RuntimeException("DEPRULE expected after DEPENDS");
                    String dependencyRuleString = line.substring("deprule".length()).replaceAll("\\s", "");
                    while (!dependencyRuleString.isEmpty()) {
                        char c = dependencies == null ? 0 : dependencyRuleString.charAt(0);
                        if (c > 0) dependencyRuleString = dependencyRuleString.substring(1);

                        String dependencyLabel = getNextGroup(dependencyRuleString).toLowerCase();
                        dependencyRuleString = dependencyRuleString.substring(dependencyLabel.length());
                        if (!preDep.containsKey(dependencyLabel))
                            throw new IOException("Dependency " + dependencyLabel + " not declared");
                        Dependency dependency = preDep.get(dependencyLabel);
                        if (c == 0) {
                            dependencies = new DepRule();
                            dependencies.addDependencyOr(dependency);
                        } else if (c == '|') {
                            dependencies.addDependencyOr(dependency);
                        } else if (c == '&') {
                            dependencies.addDependencyAnd(dependency);
                        } else
                            throw new RuntimeException("Invalid dependency rule character");
                    }
                    if (dependencies != null)
                        thisModule.applyDependencies(dependencies);
                } else if (tokens.get(0).equalsIgnoreCase("setup")) {
                    try {
                        thisModule.setSetupScript(tokens.get(1));
                    } catch (FileNotFoundException e) {
                        System.err.println(e.getMessage());
                    }
                } else
                    throw new IOException("Unknown Tag '" + tokens.get(0) + "' in Module Definition file");
            }
        } finally {
            if (br!=null) br.close();
        }
        if (thisModule == null)
            throw new RuntimeException("MODULE expected");
        return thisModule;
    }

    private static String getNextGroup(String depRuleString) {
        int i = depRuleString.indexOf('|');
        int j = depRuleString.indexOf('&');
        if (j < i && j >= 0) i = j;
        if (i >= 0) {
            return depRuleString.substring(0,i);
        } else {
            return depRuleString;
        }
    }

    public Collection<Module> getInstalledModules() {
        return installed;
    }

    public Collection<Module> findModulesByGroup(String group) {
        return installed.stream().filter(m->m.getAuthor().equalsIgnoreCase(group)).collect(Collectors.toSet());
    }
    public Optional<Module> findModuleExact(String group, String name) {
        return installed.stream().filter(m->m.getAuthor().equalsIgnoreCase(group) && m.getName().equalsIgnoreCase(name)).findFirst();
    }

    public void updateSingleModule(Module module) {
        installed.removeIf(m->m.getAuthor().equalsIgnoreCase(module.getAuthor()) &&
                m.getName().equalsIgnoreCase(module.getName()));
        installed.add(module);
    }
}
