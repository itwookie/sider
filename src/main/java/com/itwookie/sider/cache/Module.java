package com.itwookie.sider.cache;

import com.itwookie.sider.Version;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.Optional;

public class Module {
    private String name;
    private String author;
    private Version version;
    private DepRule dependencies = new DepRule();
    //automatically executes after .sdr 'module' command
    private File setupScript = null;

    public Module (String author, String name, String version) {
        this.author = author;
        this.name = name;
        this.version = new Version(version);
    }

    public String getAuthor() {
        return author;
    }
    public String getName() {
        return name;
    }
    public Version getVersion() {
        return version;
    }

    void applyDependencies(DepRule dependencies) {
        this.dependencies = dependencies;
    }
    public DepRule getDependencies() {
        return dependencies;
    }
    void setSetupScript(String filename) throws FileNotFoundException {
        File target = new File(getModuleDirectory(), filename);
        if (!target.exists())
            throw new FileNotFoundException("Setup script "+filename+" does not exist");
        setupScript = target;
    }
    public Optional<File> getSetupScript() {
        return Optional.ofNullable(setupScript);
    }

    @Override
    public String toString() {
        return String.format("%s %s %s", author, name, version.toString());
    }

    public File getModuleDirectory() {
        return new File("module"+File.separator+author+File.separator+name);
    }


}
