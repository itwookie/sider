package com.itwookie.sider.cache;

import com.itwookie.sider.SiderUtil;

import java.util.*;

public class DepRule {

    private LinkedList<List<Dependency>> stacks = new LinkedList<>(); //outer set are or, inner sets are and

    /** adds the dependency to the current inner list */
    public void addDependencyAnd(Dependency dependency) {
        if (stacks.isEmpty()) {
            addDependencyOr(dependency);
        } else {
            stacks.getLast().add(dependency);
        }
    }
    /** adds a new inner list and the dependency to it */
    public void addDependencyOr(Dependency dependency) {
        stacks.add(new LinkedList<>(Collections.singleton(dependency)));
    }

    public boolean satisfied(Collection<Module> modules) {
        if (stacks.isEmpty()) {
            if (SiderUtil.VERBOSE.get())
                System.out.println("No Deps");
            return true;
        }
        orSets: for (List<Dependency> orSet : stacks) {
            for (Dependency andSet : orSet) {
                Optional<Boolean> compatible = modules.stream()
                        .filter(andSet::isSameModule) //should only remain max 1 element
                        .findFirst()
                        .map(andSet::isCompatible);
                        //.reduce((s,m)->(s && m)); //if .findFirst() is skipped, this is needed
                if (!compatible.isPresent()) {// required module not found in modules
                    if (SiderUtil.VERBOSE.get())
                        System.out.println("Dep not installed");
                    continue orSets;
                }
                if (!compatible.get()) {// version is incompatible to found element
                    if (SiderUtil.VERBOSE.get())
                        System.out.println("Dep outdated");
                    continue orSets;
                }
            }
            if (SiderUtil.VERBOSE.get())
                System.out.println("Dep fine");
            return true;
        }
        return false;
    }

}
