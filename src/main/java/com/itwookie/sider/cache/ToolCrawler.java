package com.itwookie.sider.cache;

import com.itwookie.sider.SiderUtil;

import java.io.*;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

public class ToolCrawler {

    private Map<String, File> tools = new HashMap<>();
    public Set<String> getTools() {
        return tools.keySet();
    }

    private static final char[] progress = new char[]{ '/', '-', '\\', '|' };
    private static int progressi = 0;
    private static void printProgress(String element) { System.out.printf( "%c %s%s\r", progress[(progressi=(++progressi)%progress.length)], element, new String(new char[79-(element.length()+2)]).replace('\0', ' ') ); }

    public ToolCrawler() {

    }
    public void update() throws IOException {
        tools.clear();
        File toolPath = new File("tools");
        if (!toolPath.exists() || !toolPath.isDirectory())
            return;
        for (File f : toolPath.listFiles()) {
            if (!f.isDirectory()) continue;
            File toolfile = new File(f, "tool.sider");
            if (!toolfile.exists() || !toolfile.isFile()) continue;
            printProgress("Indexing tool "+f.getName());
            parseToolFile(toolfile);
        }
    }

    private void parseToolFile(File toolSiderFile) throws IOException {
        BufferedReader br = new BufferedReader(new InputStreamReader(new FileInputStream(toolSiderFile)));
        File toolDirectory = toolSiderFile.getParentFile();
        String line;
        List<String> tokens;
        while ((line=br.readLine())!=null) {
            if (line.isEmpty() || line.startsWith("#") || line.startsWith("'") || line.startsWith("//"))
                continue;

            tokens = SiderUtil.tokenize(line);
            if (tokens.get(0).equalsIgnoreCase("tool")) {
                String name = tokens.get(1).toUpperCase();
                String relPath = "tools"+File.separator+toolDirectory.getName()+File.separator+tokens.get(2);
                File test = new File(relPath);
                if (!test.exists() && test.isFile())
                    throw new IOException("Tool "+name+" not found, probably corrupted");
                tools.put(name, new File(relPath).getAbsoluteFile());
            } else if (tokens.get(0).equalsIgnoreCase("env")) {
                if (tokens.get(1).equalsIgnoreCase("set")) {
                    SiderUtil.environmentSet(tokens.get(2), tokens.get(3));
                } else if (tokens.get(1).equalsIgnoreCase("add")) {
                    SiderUtil.environmentAdd(tokens.get(2), tokens.get(3));
                } else if (tokens.get(1).equalsIgnoreCase("addpath")) {
                    SiderUtil.environmentAddPath(tokens.get(2), new File(toolSiderFile.getParentFile(), tokens.get(3)).getPath());
                } else
                    throw new IOException("Invalid env transformer");

            } else if (tokens.get(0).equalsIgnoreCase("includes")) {
                File src = new File(toolSiderFile.getParentFile(), tokens.get(1));
                if (!src.exists() || !src.isDirectory())
                    throw new IOException("Can't copy headers from not-include-directory "+tokens.get(1));
                File dest = new File("include");
                if (!dest.exists() && !dest.mkdirs())
                    throw new IOException("Could not create shared include directory");
                SiderUtil.copy_directory(src, dest, true);
            } else if (tokens.get(0).equalsIgnoreCase("libs")) {
                File src = new File(toolSiderFile.getParentFile(), tokens.get(1));
                if (!src.exists() || !src.isDirectory())
                    throw new IOException("Can't copy headers from not-include-directory "+tokens.get(1));
                File dest = new File("libs");
                if (!dest.exists() && !dest.mkdirs())
                    throw new IOException("Could not create shared include directory");
                SiderUtil.copy_directory(src, dest, true);
            } else if (tokens.get(0).equalsIgnoreCase("bin")) {
                File src = new File(toolSiderFile.getParentFile(), tokens.get(1));
                if (!src.exists() || !src.isDirectory())
                    throw new IOException("Can't copy headers from not-include-directory "+tokens.get(1));
                File dest = new File("bin");
                if (!dest.exists() && !dest.mkdirs())
                    throw new IOException("Could not create shared include directory");
                SiderUtil.copy_directory(src, dest, true);
            } else
                throw new IOException("Tool Definition file corrupted or outdated");
        }
    }

    public File getTool(String name) {
        name = name.toUpperCase();
        if (tools.containsKey(name))
            return tools.get(name);
        else
            throw new RuntimeException("Engine tried to access non-registered tool");
    }
    public boolean isKnown(String toolName) {
        return tools.containsKey(toolName.toUpperCase());
    }

}
