package com.itwookie.sider.cache;

import com.itwookie.sider.SiderUtil;
import com.itwookie.sider.Version;

public class Dependency {
    private String author;
    private String name;
    private Version minVersion;
    private Version maxVersion;
    private boolean minvi, maxvi, exact;
    private String source;

    /** version range specifies a range between two versions in brackets.
     * round brackets denote exclusive, square brackets denote inclusive.
     * one version is required, versions are separated via comma. omitting
     * the brackets requires the string to match a single version and will
     * only match that exact version.
     * @see <a href="https://maven.apache.org/enforcer/enforcer-rules/versionRanges.html">Magen VersionRange</a> */
    public Dependency(String author, String name, String versionRange) {
        this.author = author;
        this.name = name;
        versionRange = versionRange.replaceAll("\\s", "");
        char a = versionRange.charAt(0), b = versionRange.charAt(versionRange.length()-1);
        if ((a == '[' || a == '(') && (b == ']' || b == ')')) {
            minvi = a == '[';
            maxvi = b == ']';
            exact = false;
            int i = versionRange.indexOf(',');
            if (i<0)
                throw new IllegalArgumentException("Version Range invalid");
            String vs = versionRange.substring(1, i);
            if (vs.isEmpty()) {
                minVersion = null;
            } else {
                minVersion = new Version(vs);
            }
            vs = versionRange.substring(i+1, versionRange.length()-1);
            if (vs.isEmpty()) {
                maxVersion = null;
            } else {
                maxVersion = new Version(vs);
            }
            if (minVersion == null && maxVersion == null)
                throw new IllegalArgumentException("Version Range invalid");
        } else {
            exact = true;
            minVersion = new Version(versionRange);
        }
    }

    public void setSearchSource(String soruce) {
        this.source = soruce;
    }

    /** Checks if the specified module is a compatible and valid version for this dependency.
     * Ensure that passed module hase same author, name! */
    public boolean isCompatible(Module module) {
        if (SiderUtil.VERBOSE.get())
            System.out.printf("%s %s v%s ", module.getAuthor(), module.getName(), module.getVersion().toString());
        if (!module.getAuthor().equalsIgnoreCase(author)) return false;
        if (!module.getName().equalsIgnoreCase(name)) return false;
        if (exact) {
            return (module.getVersion().compareTo(minVersion)==0);
        } else {
            if (minVersion!=null) {
                int c = module.getVersion().compareTo(minVersion);
                if (SiderUtil.VERBOSE.get())
                    System.out.printf("min %s %c ", minVersion.toString(), c<0?'<':(c>0?'>':'='));
                if ((minvi && c < 0) || c <= 0) {
                    System.out.println();
                    return false;
                }
            }
            if (maxVersion!=null) {
                int c = module.getVersion().compareTo(minVersion);
                if (SiderUtil.VERBOSE.get())
                    System.out.printf("max %s %c ", minVersion.toString(), c<0?'<':(c>0?'>':'='));
                if ((maxvi && c < 0) || c <= 0) {
                    System.out.println();
                    return false;
                }
            }
        }
        if (SiderUtil.VERBOSE.get())
            System.out.println("passed ");
        return true;
    }

    public boolean isSameModule(Module module) {
        return module.getAuthor().equalsIgnoreCase(author) && module.getName().equalsIgnoreCase(name);
    }
}
