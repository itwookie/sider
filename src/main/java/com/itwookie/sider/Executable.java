package com.itwookie.sider;

import com.itwookie.sider.script.ScriptEngine;

import java.io.*;
import java.util.Optional;
import java.util.concurrent.atomic.AtomicBoolean;

public class Executable {
    public static final Version VERSION = new Version("0.1");

    public static AtomicBoolean EXIT_FLAG = new AtomicBoolean(false);

    public static void main(String[] args) {
        try {
            setupStreamMirrors();
        } catch (Exception e) {
            System.err.println("Could not set up file logging");
            System.exit(-1);
        }

        Arguments pargs = new Arguments(
                new Arguments.Switch("Display help", "help", "?"),
                new Arguments.Switch("Print version and exit", "version"),
                new Arguments.Switch("Print version and exit", "v", "verbose"),
                new Arguments.Text("A sriptfile to execute", "script")
        );
        try {
            pargs.parse(args);
        } catch (Exception e) {
            printHelp(e.getMessage(), -1);
        }
        if (pargs.getSwitch("help").get().isSet()) {
            printHelp(null, 0);
        } else if (pargs.getSwitch("version").get().isSet()) {
            System.out.println(VERSION);
        } else {
            if (pargs.getSwitch("verbose").get().isSet())
                SiderUtil.VERBOSE.set(true);

            Optional<String> script = pargs.getText("script").get().getText();
            if (script.isPresent())
                try {
                    new ScriptEngine(script.get()).run();
                } catch (Exception e) {
                    e.printStackTrace();
                    System.exit(-1);
                } finally {
                    EXIT_FLAG.set(true);
                }
            else { //no file specified
                ScriptEngine engine = null;
                BufferedReader br = null;
                try {
                    engine = new ScriptEngine();
                    br = new BufferedReader(new InputStreamReader(System.in));
                } catch (Exception e) {
                    e.printStackTrace();
                    System.exit(-1);
                    EXIT_FLAG.set(true);
                }
                String line;
                System.out.print(": ");
                try {
                    while ((line = br.readLine()) != null) {
                        try {
                            if (!engine.parseLine(line))
                                break;
                        } catch (ScriptError se) {
                            se.printMessage(System.err);
                        }
                        System.out.print(": ");
                    }
                } catch (IOException e) {
                    e.printStackTrace();
                } finally {
                    EXIT_FLAG.set(true);
                }
            }
        }
        System.exit(0);
    }

    private static void printHelp(String optionalMessage, int exitCode) {
        System.out.println("  [Si]mple\n  [De]pendency\n  [R]esolver\n");
        if (optionalMessage != null)
            System.out.println(optionalMessage);
        System.out.println("Usage: JAVA [[[-v] script]|-version]");
        System.out.flush();
        EXIT_FLAG.set(true);
        System.exit(exitCode);
    }
    private static void setupStreamMirrors() throws IOException {

        //transform std out
        PrintStream sysout = System.out;
        PipedInputStream outinstream = new PipedInputStream();
        PipedOutputStream outoutstream = new PipedOutputStream(outinstream);
        PrintStream cout = new PrintStream(outoutstream);
        System.setOut(cout);

        //transform std err
        PrintStream syserr = System.err;
        PipedInputStream errinstream = new PipedInputStream();
        PipedOutputStream erroutstream = new PipedOutputStream(errinstream);
        PrintStream cerr = new PrintStream(erroutstream);
        System.setErr(cerr);

        //create logger file stream
        FileOutputStream fos = new FileOutputStream(new File("latest.log"));

        //async ready both and write
        //since this thread has no exit condition on it's own it relies on
        //the main thread to System.exit
        new Thread() {
            @Override
            public void run() {
                byte[] buffer = new byte[512];
                int r;
                for (;!EXIT_FLAG.get();) {
                    try {
                        if (errinstream.available() > 0) {
                            while (errinstream.available() > 0) {
                                r = errinstream.read(buffer);
                                syserr.write(buffer, 0, r);
                                fos.write(buffer, 0, r);
                            }
                            syserr.flush();
                            fos.flush();
                        }
                        if (outinstream.available() > 0) {
                            while (outinstream.available() > 0) {
                                r = outinstream.read(buffer);
                                sysout.write(buffer, 0, r);
                                fos.write(buffer, 0, r);
                            }
                            sysout.flush();
                            fos.flush();
                        }
                    } catch (Exception e){/**/}
                    Thread.yield();
                }
            }
        }.start();
    }
}
