package com.itwookie.sider;

import com.itwookie.sider.cache.ModuleCrawler;
import com.itwookie.sider.cache.ToolCrawler;

import java.io.*;
import java.net.HttpURLConnection;
import java.net.URL;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.StandardCopyOption;
import java.util.*;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.regex.Pattern;
import java.util.stream.Collectors;

public class SiderUtil {

    public static ModuleCrawler modules = new ModuleCrawler();
    public static ToolCrawler tools = new ToolCrawler();
    public static Map<String, String> envOverwrite = new HashMap<>(System.getenv());
    static {
        environmentSet("SIDERINCLUDE", new File("include").getAbsolutePath());
        environmentSet("SIDERLIBS", new File("libs").getAbsolutePath());
        environmentSet("SIDERBIN", new File("bin").getAbsolutePath());
        environmentSet("SIDERVERSION", Executable.VERSION.toString());
        environmentAddPath("PATH", new File("bin").getAbsolutePath());
    }

    public static AtomicBoolean VERBOSE = new AtomicBoolean(false);
    public static Path TEMP_DIR = Paths.get(".", "downloads", "tmp");
    public static Path GLOBAL_INCLUDE_DIR = Paths.get(".", "include");
    public static Path GLOBAL_LIBS_DIR = Paths.get(".", "libs");
    public static Path GLOBAL_BIN_DIR = Paths.get(".", "bin");

    public static String[] token(String input) {
        int si = 1;
        if (input.isEmpty())
            throw new RuntimeException("String expected");
        else if (input.charAt(0) == '"') {
            do {
                si=input.indexOf('"', si);
                if (si<0)
                    throw new RuntimeException("Quoted String does not end");
                else if (input.charAt(si-1)!='\\') {
                    if (si == input.length()-1) //end quote is last character
                        return new String[]{input.substring(1, si).replace("\\\"", "\""), ""};
                    else if (input.charAt(si+1)==' ')
                        return new String[]{input.substring(1, si).replace("\\\"", "\""), input.substring(si+2)};
                    else
                        throw new RuntimeException("Quoted string was not followed by space");
                }
            } while (si<input.length());
        } else {
            si=input.indexOf(' ', si);
            if (si < 0)
                return new String[]{input, ""};
            else
                return new String[]{input.substring(0, si), input.substring(si+1)};
        }
        throw new RuntimeException("Unexpected state");
    }
    public static List<String> tokenize(String s) {
        List<String> tokens = new LinkedList<>();
        if (s.isEmpty()) return tokens;
        String[] ab = token(s);
        while (!ab[1].isEmpty()) {
            tokens.add(ab[0]);
            ab = token(ab[1]);
        }
        tokens.add(ab[0]);
        return tokens;
    }

    public static String resolveRegexPathAll(Path cwd, String multiArgs) {
        List<String> collector = new LinkedList<>();
        for (String a : tokenize(multiArgs)) {
//            System.out.println("Next Token: "+a);
            if (a.contains("{{") && a.contains("}}")) {
                String tmp = resolveRegexPath(cwd, a).getAbsolutePath();
//                System.out.println("->> "+tmp);
                if (tmp.indexOf(' ')>=0)
                    collector.add('"'+tmp+'"');
                else
                    collector.add(tmp);
            } else
                collector.add(a);
        }
        return concatArgs(collector);
    }
    public static File resolveRegexPath(Path cwd, String path) {
        List<String> elements = new LinkedList<>();
        //extended split, with {{}}-quotes
        StringBuilder sb = new StringBuilder();
        boolean inQ=false;
        for (char c : path.toCharArray()) {
            if ((c=='/' || c=='\\') && !inQ) {
                elements.add(sb.toString());
                sb.setLength(0);
            } else {
                if (c == '{' && sb.toString().equals("{") && !inQ) {
                    inQ = true;
                } else if (c == '}' && sb.charAt(sb.length()-1)=='}') {
                    inQ = false;
                }
                sb.append(c);
            }
        }
        if (sb.length()>0)
            elements.add(sb.toString());

        //search for pattern elements
        File f = cwd.toFile();
        elements: for (String elem : elements) {
//            System.out.println("  "+elem);
            if (elem.startsWith("{{") && elem.endsWith("}}")) {
                Pattern p = Pattern.compile(elem.substring(2,elem.length()-2));
//                System.out.println("Pattern: "+elem.substring(2,elem.length()-2));
                for (File sub : f.listFiles()) {
                    if (p.matcher(sub.getName()).matches()) {
                        f = sub;
                        continue elements;
                    }
                }
                throw new RuntimeException("Could not resolve {{"+elem+"}} in "+f.getAbsolutePath());
            } else {
                f = new File(f, elem);
            }
        }
        return f.getAbsoluteFile();
    }

    public static String concatArgs(String... args) {
        return concatArgs(Arrays.asList(args));
    }
    public static String concatArgs(List<String> args) {
        return String.join(" ",
                args.stream()
                        .map(a->a.indexOf(' ')>=0 ? ('"'+a.replace("\"", "\\\"")+'"'):a)
                        .collect(Collectors.toList()));
    }

    public static ProcessBuilder tool(String name, Path workDirectory, String argString) throws IOException {
        name = name.toUpperCase();
        if (!tools.isKnown(name))
            throw new RuntimeException("Unknown tool '"+name+"'");
        ProcessBuilder p = new ProcessBuilder();
        List<String> cmdArgs = new LinkedList<>(tokenize(resolveRegexPathAll(workDirectory, argString)));
        cmdArgs.add(0, tools.getTool(name).getPath());
        p.command(cmdArgs);

        File wdf = workDirectory.toAbsolutePath().toFile();
        if (!wdf.isDirectory() || !wdf.exists())
            if (!wdf.mkdirs())
                throw new IOException("Could not create work directory "+wdf.getAbsolutePath());
        p.directory(wdf);
        System.out.println("Calling >"+ String.join("< >", cmdArgs)+"<  from "+wdf.getAbsolutePath());
        return p;
    }

    public static File download(Path folder, URL src) throws IOException {
        String filename = src.getPath();
        filename = filename.substring(filename.lastIndexOf('/'));
        File target = new File(folder.toFile().getAbsolutePath(), filename);
        System.out.println("Downloading "+src.toString()+" to "+target.getAbsolutePath());
        HttpURLConnection con = (HttpURLConnection)src.openConnection();
        con.setDoInput(true);
        if (con.getResponseCode()!= 200)
            throw new IOException("Could not download file from '"+src.toString()+"'");
        String v = con.getHeaderField("Content-Length");
        long expected = v != null ? Long.parseLong(v) : 0;

        InputStream in = con.getInputStream();
        OutputStream out = new FileOutputStream(target);
        streamCpy(in, out, expected);
        return target;
    }

    public static void extract(File fileToExtract, Path targetDirectory) throws IOException {
        System.out.println("Extracting "+fileToExtract.getAbsolutePath()+" to "+targetDirectory.toAbsolutePath().toString());
        if (run(tool("7zip", targetDirectory, SiderUtil.concatArgs("x", fileToExtract.getAbsolutePath(), "-r", "-aoa")), null)!=0)
            throw new RuntimeException("Extraction failed");
    }

    public static int run(Process process) {
        while(process.isAlive()) {
            try {
                return process.waitFor();
            } catch (Exception e) {
                /**/
            }
        }
        return process.exitValue();
    }
    /** sets modified environment for process builder and executes
     * @param environment can be null to use system default */
    public static int run(ProcessBuilder process, Map<String, String> environment) throws IOException {
        if (environment!=null) process.environment().putAll(environment);
        process.inheritIO();
        return run(process.start());
    }

    /** copies all bytes from in to out, then flushes and closes the streams */
    public static void streamCpy(InputStream in, OutputStream out, long expectedBytes) throws IOException {
        long receivedBytes = 0;
        byte[] buffer = new byte[1024]; int r;
        int pp = 0; //percent previous
        while ((r=in.read(buffer))>=0) {
            receivedBytes+=r;
            out.write(buffer,0,r);
            if (expectedBytes>0) {
                int cp = (int)(receivedBytes / (expectedBytes / 100)); //current percent progress
                if (cp != pp) { //only print if progress changed, not optimal, but better than spamming the console every 1kb
                    pp = cp;
                    System.out.printf("[%03d%%] [", pp);
                    int n = pp/5, i = 0;
                    for (; i < n; i++) System.out.print('#');
                    for (; i < 20; i++) System.out.print(' ');
                    System.out.printf("] %d/%d kB\r", receivedBytes >>> 10, expectedBytes >>> 10);
                }
            }
        }
        System.out.println();
        out.flush();
        out.close();
        in.close();
    }

    public static void move_directory(File source, File target) throws IOException {
        for (File file : source.listFiles()) {
//            if (VERBOSE.get()) System.out.println("> "+file.getAbsolutePath());
            if (file.isDirectory()) {
                File subDir = new File(target, file.getName());
                if (!subDir.exists() && !subDir.mkdirs())
                    throw new RuntimeException("Could not create sub directory");
                move_directory(file, subDir);
            } else if (file.isFile()) {
                Files.move(file.toPath(), new File(target, file.getName()).toPath(), StandardCopyOption.REPLACE_EXISTING);
            }
        }
    }
    /** weak skips any already existing file */
    public static void copy_directory(File source, File target, boolean weak) throws IOException {
        for (File file : source.listFiles()) {
//            if (VERBOSE.get()) System.out.println("> "+file.getAbsolutePath());
            if (file.isDirectory()) {
                File subDir = new File(target, file.getName());
                if (!subDir.exists() && !subDir.mkdirs())
                    throw new RuntimeException("Could not create sub directory");
                copy_directory(file, subDir, weak);
            } else if (file.isFile()) {
                File dest = new File(target, file.getName());
                if (!weak || !dest.exists())
                    Files.copy(file.toPath(), dest.toPath(), StandardCopyOption.REPLACE_EXISTING, StandardCopyOption.COPY_ATTRIBUTES);
            }
        }
    }
    public static void delete_file(File target) {
        if (!target.exists()) return;
        if (target.isFile()) {
            if (!target.delete())
                throw new RuntimeException("Could not delete file "+target.getAbsolutePath());
        } else if (target.isDirectory()) {
            delete_file_tree(target);
        }
    }
    /** delete file tree including root */
    private static void delete_file_tree(File treeRoot) {
        for (File file : treeRoot.listFiles()) {
            if (file.isDirectory()) {
                delete_file_tree(file);
            } else
            if (!file.delete())
                throw new RuntimeException("Could not delete file "+ file.getAbsolutePath());
        }
        if (!treeRoot.delete())
            throw new RuntimeException("Could not delete folder "+ treeRoot.getAbsolutePath());
    }

    public static void environmentSet(String key, String value) {
        //find current case for key, so key stays case insensitive
        String cikey = envOverwrite.keySet().stream().filter(k->k.equalsIgnoreCase(key)).findFirst().orElse(key);
        envOverwrite.put(cikey, value);
    }
    /**
     * @param path has to be relative to the sider work directory or absolute
     */
    public static void environmentAddPath(String key, String path) {
        //find current case for key, so key stays case insensitive
        String cikey = envOverwrite.keySet().stream().filter(k->k.equalsIgnoreCase(key)).findFirst().orElse(key);
        Set<String> pathcollection = new HashSet<>(Arrays.asList(envOverwrite.getOrDefault(cikey, "").split(File.pathSeparator)));
        pathcollection.remove("");
        pathcollection.add(Paths.get(path).toAbsolutePath().toString());
        envOverwrite.put(cikey, String.join(File.pathSeparator, pathcollection));
    }
    /** blindly appends value to the environment variable */
    public static void environmentAdd(String key, String value) {
        //find current case for key, so key stays case insensitive
        String cikey = envOverwrite.keySet().stream().filter(k->k.equalsIgnoreCase(key)).findFirst().orElse(key);
        envOverwrite.put(cikey, envOverwrite.getOrDefault(cikey, "")+value);
    }
    public static Optional<String> environmentGet(String key) {
        //find current case for key, so key stays case insensitive
        return envOverwrite.keySet().stream()
                .filter(k->k.equalsIgnoreCase(key))
                .findFirst()
                .map(cikey->envOverwrite.get(cikey));
    }

}
