package com.itwookie.sider;

import java.io.PrintStream;

public class ScriptError extends RuntimeException {

    public ScriptError(String message) {
        super("Err: " + message);
    }

    public ScriptError(Throwable cause) {
        super("Err: "+cause.getMessage(), cause);
    }

    public ScriptError(String message, Throwable cause) {
        super("Err: " + message, cause);
    }

    public ScriptError(Throwable cause, String script, int line) {
        super("SE " + script + "@" + line + ": " + cause.getMessage(), cause);
    }

    public ScriptError(String message, String script, int line) {
        super("SE " + script + "@" + line + ": " + message);
    }

    public ScriptError(String message, Throwable cause, String script, int line) {
        super("SE " + script + "@" + line + ": " + message, cause);
    }

    public void printMessage(PrintStream writer) {
        Throwable t = this;
        while ( t != null ) {
            writer.println(t.getMessage());
            t = t.getCause();
        }
    }

}
