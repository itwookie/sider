package com.itwookie.sider;


import java.util.LinkedList;
import java.util.List;
import java.util.Optional;


/** arg parser for unordered arguments */
public class Arguments {

    interface Element {}

    public static class Switch implements Element {
        private String desc;
        private String[] aliases;
        private boolean set=false;
        public Switch(String description, String... aliases) {
            this.desc = description;
            this.aliases = aliases;
        }
        private boolean isAlias(String element) {
            return arrayContains(aliases, element);
        }
        private void set() {
            set=true;
        }
        public boolean isSet() {
            return set;
        }

        public String getFirstAlias() {
            return aliases[0];
        }
        public String getDesc() {
            return String.format("  %s\n    %s", String.join(", ", aliases), desc);
        }
    }

    public static class Text implements Element {
        private String desc;
        private String name;
        private String value;
        public Text(String description, String name) {
            this.desc = description;
            this.name = name;
        }
        private void setValue(String text) {
            this.value = text;
        }
        public Optional<String> getText() {
            return Optional.ofNullable(value);
        }

        public String getName() {
            return name;
        }
        public String getDesc() {
            return String.format("  %s\n    %s", name, desc);
        }
    }

    private List<Switch> switches = new LinkedList<>();
    private List<Text> texts = new LinkedList<>();
    public Arguments(Element... elements) {
        for (Element element : elements) {
            if (element instanceof Switch) {
                switches.add((Switch)element);
            } else if (element instanceof Text) {
                texts.add((Text)element);
            }
        }
    }

    public Optional<Switch> getSwitch(String name) {
        return switches.stream().filter(s->s.isAlias(name)).findFirst();
    }
    public Optional<Text> getText(String name) {
        return texts.stream().filter(f->f.getName().equalsIgnoreCase(name)).findFirst();
    }
    /** @return the next unset file object in the texts list */
    private Optional<Text> nextText() {
        return texts.stream().filter(f->!f.getText().isPresent()).findFirst();
    }

    private static <T> boolean arrayContains(T[] a, T e) {
        for (T x : a) if (x.equals(e)) return true; return false;
    }

    public void parse(String... args) throws Exception {
        for (int i = 0;i<args.length;i++) {
            String arg = args[i];
            if (arg.startsWith("-")) {
                arg = arg.substring(1);
                Optional<Switch> sw = getSwitch(arg);
                if (sw.isPresent()) {
                    sw.get().set();
                } else {
                    //handle options
                    throw new Exception("Unknown switch or option -"+arg);
                }
            } else { //text
                nextText().orElseThrow(()->new Exception("Too many arguments"))
                        .setValue(arg);
            }
        }
    }

}
