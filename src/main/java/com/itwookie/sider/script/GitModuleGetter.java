package com.itwookie.sider.script;

import com.itwookie.sider.ScriptError;
import com.itwookie.sider.SiderUtil;
import com.itwookie.sider.cache.Module;
import com.itwookie.sider.cache.ModuleCrawler;

import java.io.File;
import java.io.IOException;
import java.nio.file.Path;

public class GitModuleGetter extends ModuleGetter {

    public GitModuleGetter() {

    }

    @Override
    public Module getModule(String group, String name, String tag, String source) throws IOException {
        //create module dir
        Module mod = new Module(group, name, tag);
        if (!askReinstall(mod)) return null;
        File moduleDir = mod.getModuleDirectory();
        if (moduleDir.exists())
            SiderUtil.delete_file(moduleDir);
        if (!moduleDir.mkdirs())
            throw new RuntimeException("Could not create module directory");
        Path cwd = moduleDir.toPath();

        try {
            if (SiderUtil.run(SiderUtil.tool("git", cwd, "clone -b "+tag+" --single-branch "+source+" ."), null)!=0)
                throw new RuntimeException("Could not clone tag "+tag);
            //detach from git by removing git dir
            SiderUtil.delete_file(new File(moduleDir, ".git"));
        } catch (IOException e) {
            throw new RuntimeException("Retrieving module from git repository failed", e);
        }

        //look into module dir for more complete module.sider
        File md = new File(moduleDir, "module.sider");
        if (md.exists()) {
            try {
                mod = ModuleCrawler.parseModuleDefinition(md);
            } catch (IOException|RuntimeException e) {
                new ScriptError("Could not load module.sider for "+mod.getAuthor()+"-"+mod.getName(), e).printMessage(System.err);
            }
        } else {
            System.out.println("Module "+mod.getAuthor()+"-"+mod.getName()+" does not ship a module.sider, you'll have to build manually!");
        }
        return mod;
    }
}
