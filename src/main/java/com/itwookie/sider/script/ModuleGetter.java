package com.itwookie.sider.script;

import com.itwookie.sider.SiderUtil;
import com.itwookie.sider.cache.Module;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.Optional;

public abstract class ModuleGetter {

    /**
     * Gets the module command preparsed into the arguments supplied.<br>
     * Note that for sources that support named versions or tags ONLY version strings are supported!
     * While this may cripple the ability to download arbitrary sources, it ensures consistency
     * (and makes things easier for me)<br>
     * Has to return a Module build from module.sider (or args) on success,
     * or null if fetching fails without exception.<br>
     * Don't forget to invoke .askReinstall(Module module) - this will automatically remove old install files if a
     * version was previously installed and the user choose to overwrite. will always return true if no install present.
     * @param author the module author (person or group)
     * @param name the module name
     * @param version the module version string
     * @param source is the full specified from part from the module command
     * @return a Module if setting up the module succeeded
     */
    public abstract Module getModule(String author, String name, String version, String source) throws IOException;

    /**
     * this will automatically remove old install files if a version was previously installed and the user
     * choose to overwrite. will always return true if no install present.
     * @param tempModule a module that can be a temporary instance to transmit author, name and version
     */
    protected boolean askReinstall(Module tempModule) {
        Optional<Module> mod = SiderUtil.modules.findModuleExact(tempModule.getAuthor(), tempModule.getName());
        if (mod.isPresent()) {
            Boolean answer = null;
            BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
            while (answer == null) {
                System.out.printf("Module %s-%s is already installed with version %s.%sDo you want to reinstall this module? (Y/N) ", mod.get().getAuthor(), mod.get().getName(), mod.get().getVersion(), System.lineSeparator());
                try {
                    String line = br.readLine();
                    char l = Character.toLowerCase(line.charAt(0));
                    if (l=='y') answer = true;
                    else if (l=='n') answer = false;
                } catch (Exception ignore) {}
            }
            if (!answer) return false;
            SiderUtil.delete_file(mod.get().getModuleDirectory());
        }
        return true;
    }

    public static ModuleGetter getForType(String source) {
        if (source.endsWith(".git")) {
            return new GitModuleGetter();
        } else if (source.endsWith(".zip")||
                    source.endsWith(".gz")||
                    source.endsWith(".gzip")||
                    source.endsWith(".bz2")||
                    source.endsWith(".bzip2")||
                    source.endsWith(".tar")||
                    source.endsWith(".rar")||
                    source.endsWith(".7z")) {
            return new ZipModuleGetter();
        }
        throw new IllegalArgumentException("Can't find module at "+source);
    }

}
