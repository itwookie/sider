package com.itwookie.sider.script;

import com.itwookie.sider.ScriptError;
import com.itwookie.sider.SiderUtil;
import com.itwookie.sider.cache.Module;
import com.itwookie.sider.cache.ModuleCrawler;

import java.io.File;
import java.io.IOException;
import java.net.URL;

public class ZipModuleGetter extends ModuleGetter {

    public ZipModuleGetter() {

    }

    @Override
    public Module getModule(String group, String name, String tag, String source) throws IOException {
        //create module dir
        Module mod = new Module(group, name, tag);
        if (!askReinstall(mod)) return null;
        File moduleDir = mod.getModuleDirectory();
        if (moduleDir.exists())
            SiderUtil.delete_file(moduleDir);
        if (!moduleDir.mkdirs())
            throw new RuntimeException("Could not create module directory");

        File zipFile;
        //first downlaod, not local
        if (source.startsWith("http://") || source.startsWith("https://")) {
            zipFile = SiderUtil.download(SiderUtil.TEMP_DIR, new URL(source));
            if (zipFile.getName().contains(".tar.")) { //probably tar-ball
                if (SiderUtil.run(SiderUtil.tool("7zip", SiderUtil.TEMP_DIR, SiderUtil.concatArgs("u", zipFile.getAbsolutePath(), "-aoa")), SiderUtil.envOverwrite)!=0)
                    throw new IOException("Could not unpack tar-ball");
                //tar-ball contained filed have one file extension less
                zipFile.delete();
                zipFile = new File(SiderUtil.TEMP_DIR.toFile(), zipFile.getName().substring(0, zipFile.getName().lastIndexOf('.')));
            }
        } else {
            zipFile = new File(source);
        }

        //unpack to module directory
        if (SiderUtil.run(SiderUtil.tool("7zip", moduleDir.toPath(), SiderUtil.concatArgs("x", zipFile.getAbsolutePath(), "-r", "-aoa")), SiderUtil.envOverwrite)!=0)
            throw new IOException("Could not unpack module archive");

        //look into module dir for more complete module.sider
        File md = new File(moduleDir, "module.sider");
        if (md.exists()) {
            try {
                mod = ModuleCrawler.parseModuleDefinition(md);
            } catch (IOException|RuntimeException e) {
                new ScriptError("Could not load module.sider for "+mod.getAuthor()+"-"+mod.getName(), e).printMessage(System.err);
            }
        } else {
            System.out.println("Module "+mod.getAuthor()+"-"+mod.getName()+" does not ship a module.sider, you'll have to build manually!");
        }
        return mod;
    }
}
