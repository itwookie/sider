package com.itwookie.sider.script;

import com.itwookie.sider.ScriptError;
import com.itwookie.sider.SiderUtil;
import com.itwookie.sider.cache.Module;
import com.itwookie.sider.cache.ModuleCrawler;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;

import java.io.*;
import java.net.URL;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.StandardCopyOption;
import java.util.*;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class ScriptEngine {
    private BufferedReader reader;
    private String fn = null;

    private String var_answer = "";

    private Map<File, PrintWriter> fprintCache = new HashMap<>();
    private PrintWriter fopenHandle = null;

    private Path cwd = Paths.get(".");

    public ScriptEngine(String filename) {
        this(filename, null);
    }
    public ScriptEngine(String filename, Path workDirectory) {
        try { SiderUtil.modules.update(); } catch (Exception e) { e.printStackTrace(); }
        try { SiderUtil.tools.update(); } catch (Exception e) { e.printStackTrace(); }

        if (workDirectory != null)
            cwd = workDirectory;
        try {
            File file = new File(filename);
            fn = file.getName();
            reader = new BufferedReader(new InputStreamReader(new FileInputStream(file)));
        } catch (FileNotFoundException e) {
            System.err.println("Script file '"+filename+"' not found");
            System.exit(-1);
        }
    }
    /** for interactive mode */
    public ScriptEngine() {
        System.out.print("Indexing...\r");

        try { SiderUtil.modules.update(); } catch (Exception e) { e.printStackTrace(); }
        try { SiderUtil.tools.update(); } catch (Exception e) { e.printStackTrace(); }

        System.out.println("-= [ SiDeR Interactive Shell ] =-");
    }

    private static final boolean isSys64 = System.getProperty("os.name").contains("Windows")
            ? (System.getenv("ProgramFiles(x86)") != null)
            : (System.getProperty("os.arch").contains("64"));

    private static final boolean isWin = System.getProperty("os.name").toLowerCase().contains("win");
    private static final boolean isMac = System.getProperty("os.name").toLowerCase().contains("max");
    private static final boolean isUnix = System.getProperty("os.name").toLowerCase().contains("nix") ||
                                            System.getProperty("os.name").toLowerCase().contains("nux") ||
                                            System.getProperty("os.name").toLowerCase().contains("aix");

    public void run() throws IOException, ScriptError {
        if (reader == null)
            throw new RuntimeException("Engine was started in interactive mode!");
        String line;
        int ic = 1;
        while ((line=reader.readLine())!=null) {
            if (!parseLine(line, ic++))
                break;
        }
        strategy_fclose();
    }

    public boolean parseLine(String line) throws ScriptError {
        if (reader != null)
            throw new RuntimeException("Engine was not started for interaction");
        return parseLine(line, 0);
    }
    /** @return true if execution shall continue */
    private boolean parseLine(String line, int lineNo) throws ScriptError {
        String part;
        String[] consumed;
        if (fopenHandle != null) {
            if (line.equalsIgnoreCase("fclose")) {
                strategy_fclose();
            } else {
                fopenHandle.write(line);
                fopenHandle.write(System.lineSeparator());
            }
        } else if (!(line.isEmpty() || line.startsWith("#") || line.startsWith("'") || line.startsWith("//"))) {
            consumed = SiderUtil.token(line);
            part = consumed[0]; line = consumed[1];
            if (line.contains("{{ANS}}")) {
                line = line.replace("{{ANS}}", var_answer);
            }
            try {
                while (part.startsWith("@")) {
                    if (part.equals("@64")) {
                        if (!isSys64)
                            return true;
                    } else if (part.equals("@32")) {
                        if (isSys64)
                            return true;
                    } else if (part.equals("@win")) {
                        if (!isWin)
                            return true;
                    } else if (part.equals("@mac")) {
                        if (!isMac)
                            return true;
                    } else if (part.equals("@unix")) {
                        if (!isUnix)
                            return true;
                    } else {
                        throw new IOException("Unknown conditional "+part);
                    }
                    consumed = SiderUtil.token(line);
                    part = consumed[0]; line = consumed[1];
                }
                switch (part.toLowerCase()) {
                    case "return": {
                        return false;
                    }
                    case "tool": {
                        strategy_tool(line);
                        break;
                    }
                    case "get": {
                        strategy_download(line);
                        break;
                    }
                    case "run": {
                        strategy_run(line);
                        break;
                    }
                    case "syscall": {
                        strategy_syscall(line);
                        break;
                    }
                    case "move": {
                        strategy_move(line);
                        break;
                    }
                    case "erase": {
                        strategy_erase(line);
                        break;
                    }
                    case "fopen": {
                        strategy_fopen(line);
                        break;
                    }
                    case "fprintln": {
                        strategy_fprintln(line);
                        break;
                    }
                    case "fclose": {
                        strategy_fclose();
                        break;
                    }
                    case "install": {
                        strategy_module(line);
                        break;
                    }
                    case "build": {
                        strategy_build(line);
                        break;
                    }
                    case "clean": {
                        strategy_clean();
                        break;
                    }
                    case "mkdirs": {
                        strategy_mkdirs(line);
                        break;
                    }
                    case "cwd": {
                        strategy_changecwd(line);
                        break;
                    }
                    case "search": {
                        strategy_search(line);
                        break;
                    }
                    case "rename": {
                        strategy_rename(line);
                        break;
                    }
                    default: {
                        if (part.startsWith(".")) {
                            if (SiderUtil.run(SiderUtil.tool(part.substring(1), cwd, line), SiderUtil.envOverwrite)!=0)
                                throw new IOException("Tool Execution failed");
                        } else
                            throw new IOException("Unknown command '" + part + "'");
                    }
                }
            } catch (IOException|RuntimeException e) {
                if (reader == null)
                    throw new ScriptError(e);
                else
                    throw new ScriptError(e, fn, lineNo);
            }
        }
        return true;
    }

    private void strategy_tool(String callPath) throws IOException {
        String[] ab = SiderUtil.token(callPath);
        String tname = ab[0].toUpperCase();
        ab = SiderUtil.token(ab[1]);
        if (!ab[0].equalsIgnoreCase("of"))
            throw new RuntimeException("Keyword 'OF' expected");
        String tscript = SiderUtil.token(ab[1])[0];

        System.out.println("Checking "+tname+ " of "+tscript);

        if (!SiderUtil.tools.isKnown(tname)) {
            if (!tscript.isEmpty()) {
                System.out.println("Missing Tool " + tname + ", trying to install.");
                new ScriptEngine("tools/" + tscript + ".sdr").run();
                SiderUtil.tools.update();
            } else {
                throw new RuntimeException("No script specified to install Missing Tool " + tname);
            }
        }
        var_answer = "";
    }

    /** git repos will automatically clone into /downloads/repoName/
     * other downloads requires a sibDirectory name.
     * if the url is not a zip archive, a optional querySelector may
     * be specified to find the download url from the given url.<br>
     * arg: (&lt;*.git&gt;|&lt;subdir&gt; &lt;srcUrl&gt; [CSS querySelector]) */
    private void strategy_download(String line) throws IOException {
        if (line.endsWith(".git")) {
            if (!SiderUtil.tools.isKnown("git"))
                strategy_tool("git of git");
            SiderUtil.tool("git", Paths.get(".", "downloads").toAbsolutePath(), line);
            var_answer = "downloads"+File.separator+line.substring(line.lastIndexOf('/')+1, line.lastIndexOf('-'));
        } else {
            String[] ab = SiderUtil.token(line);
            File folder = new File("downloads", ab[0]);
            if ((!folder.exists() || !folder.isDirectory()) && !folder.mkdirs())
                throw new IOException("Could not create target folder");
            ab = SiderUtil.token(ab[1]);
            URL src = new URL(ab[0]);
            File dlFile;
            Path downloadTemp = Paths.get(".", "downloads", "tmp").toAbsolutePath();
            downloadTemp.toFile().mkdirs();
            if (ab[1].isEmpty()) {
                dlFile = SiderUtil.download(downloadTemp, src);
            } else {
                Document doc = Jsoup.parse(src, 30_000);
                Element element = doc.selectFirst(SiderUtil.token(ab[1])[0]);
                if (!element.tagName().equalsIgnoreCase("a"))
                    throw new RuntimeException("Specified selector not a link");
                String fileurl = element.attr("abs:href");
                if (fileurl.isEmpty())
                    throw new RuntimeException("Specified selector returned <a> without href");
                dlFile = SiderUtil.download(downloadTemp, new URL(fileurl));
            }
            SiderUtil.extract(dlFile, folder.toPath());
            var_answer = folder.getPath();
        }
    }

    /** returns the exit code */
    private int strategy_run(String line) throws IOException {
        ProcessBuilder p = new ProcessBuilder();
        List<String> cmdArgs = SiderUtil.tokenize(SiderUtil.resolveRegexPathAll(cwd, line));
        System.out.println("Calling >"+ String.join("<, >", cmdArgs)+"<");
        p.command(cmdArgs);
        p.directory(cwd.toFile());
        return SiderUtil.run(p, SiderUtil.envOverwrite);
    }

    /** returns the exit code */
    private int strategy_syscall(String line) throws IOException {
        List<String> cmdArgs;
        if (isWin)
            cmdArgs = new LinkedList<>(Arrays.asList("cmd", "/C", SiderUtil.resolveRegexPathAll(cwd, line)));
        else if (isUnix)
            cmdArgs = new LinkedList<>(Arrays.asList("bash", "-c", SiderUtil.resolveRegexPathAll(cwd, line)));
        else
            throw new RuntimeException("syscall not supported for MAC");
        System.out.println("Calling >"+ String.join("<, >", cmdArgs)+"<");
        ProcessBuilder p = new ProcessBuilder(cmdArgs);
        p.directory(cwd.toFile());

        return SiderUtil.run(p, SiderUtil.envOverwrite);
    }

    private void strategy_move(String line) throws IOException {
        String[] ab = SiderUtil.token(line);
        File src = SiderUtil.resolveRegexPath(cwd, ab[0]);
        ab = SiderUtil.token(ab[1]);
        File dest = SiderUtil.resolveRegexPath(cwd, ab[0]);
        boolean toDir = ab[0].endsWith("/") || ab[0].endsWith("\\");

        System.out.println("Move "+src.getAbsolutePath()+" to "+dest.getAbsolutePath());
        if (dest.exists() && toDir && !dest.isDirectory())
            throw new RuntimeException("Target already exists and is not directory");

        if (src.isFile()) {
            if (toDir && !dest.exists())
                if (!dest.mkdirs())
                    throw new RuntimeException("Could not create target directory");
                else if (!toDir && !dest.getParentFile().exists())
                    if (!dest.getParentFile().mkdirs())
                        throw new RuntimeException("Could not create target directory");

            if (toDir)
                dest = new File(dest, src.getName());
            Files.move(src.toPath(), dest.toPath(), StandardCopyOption.COPY_ATTRIBUTES, StandardCopyOption.REPLACE_EXISTING);
        } else if (src.isDirectory()) {
            if (!dest.exists() && !dest.mkdirs())
                throw new RuntimeException("Could not create target directory");
            SiderUtil.move_directory(src, dest);
            SiderUtil.delete_file(src);
        }
    }

    private void strategy_erase(String line) {
        String[] ab = SiderUtil.token(line);
        File src = SiderUtil.resolveRegexPath(cwd, ab[0]);

        System.out.println("Erase "+src.getAbsolutePath());
        SiderUtil.delete_file(src);
    }
    private void strategy_rename(String line) {
        String[] ab = SiderUtil.token(line);
        File[] files = cwd.toFile().listFiles();
        String spat = ab[0];
        if (spat.startsWith("{{") && spat.endsWith("}}")) {
            line = ab[1];
            spat = spat.substring(2, spat.length() - 2);
            Pattern pat = Pattern.compile(spat);
            String target = SiderUtil.token(line)[0];
            System.out.println("Renaming files in " + cwd.toFile().getAbsolutePath() + " from " + spat + " to " + target);
            for (File file : files) {
                String name = file.getName();
                Matcher matcher = pat.matcher(name);
                if (!matcher.matches()) continue;
                String ftarget = target;
                for (int i = 1; i <= matcher.groupCount(); i++) {
                    ftarget = ftarget.replace("{{$" + i + "}}",
                            matcher.group(i) != null ? matcher.group(i) : "");
                }
                if (!name.equalsIgnoreCase(ftarget)) {
                    File dest = new File(file.getParentFile(), ftarget);
                    if (dest.exists())
                        throw new RuntimeException("Rename destination (for "+name+") "+ftarget+" already exists!");
                    if (!file.renameTo(dest))
                        throw new RuntimeException("Unable to rename file " + file.getAbsolutePath() + " to " + ftarget);
                }
            }
        } else {
            File src = new File(cwd.toFile(), spat);
            File dest = new File(cwd.toFile(), SiderUtil.token(ab[1])[0]);
            System.out.println("Renaming file " + src.getAbsolutePath() + " to " + dest.getAbsolutePath());
            if (dest.exists())
                throw new RuntimeException("Rename destination already exists");
            if (!src.renameTo(dest))
                throw new RuntimeException("Unable to rename file!");
        }
    }

    private void strategy_mkdirs(String line) throws IOException {
        String[] ab = SiderUtil.token(line);
        File target = new File(cwd.toFile(), ab[0]);

        System.out.println("MkDirs "+ab[0]);
        if (target.exists() && target.isDirectory())
            return;
        if (!target.mkdirs())
            throw new IOException("Failed to create directory");
    }

    private void strategy_fopen(String line) throws IOException {
        String[] ab = SiderUtil.token(line);
        File src = SiderUtil.resolveRegexPath(cwd, ab[0]);

        System.out.println("Writing file "+src.getAbsolutePath());
        if (src.exists() && !src.isFile())
            throw new IOException("Can't open directory as file");
        strategy_fclose();
        fopenHandle = new PrintWriter(new FileOutputStream(src));
    }
    private void strategy_fprintln(String line) throws IOException {
        String[] ab = SiderUtil.token(line);
        File src = SiderUtil.resolveRegexPath(cwd, ab[0]);

        System.out.println(src.getAbsolutePath()+" += "+ab[1]);
        if (src.exists() && !src.isFile())
            throw new IOException("Can't open directory as file");
        PrintWriter w;
        if (fprintCache.containsKey(src))
            w = fprintCache.get(src);
        else {
            w = new PrintWriter(new BufferedWriter(new FileWriter(src, true)));
            fprintCache.put(src, w);
        }
        w.append(ab[1]);
        w.append(System.lineSeparator());
    }
    private void strategy_fclose() {
        if (fopenHandle != null) {
            fopenHandle.flush();
            fopenHandle.close();
            fopenHandle = null;
        }
        for (Map.Entry<File, PrintWriter> e : fprintCache.entrySet()) {
            e.getValue().flush();
            e.getValue().close();
        }
        fprintCache.clear();
    }


    private void strategy_changecwd(String line) throws IOException {
        File f = SiderUtil.resolveRegexPath(cwd, SiderUtil.token(line)[0]);
        Path target = f.getAbsoluteFile().toPath();
        System.out.println("Changin CWD to "+target.toString());
        if (!f.exists() || f.isFile())
            throw new IOException("Target does not exist or is not directory!");
        cwd = target;
    }

    private Module strategy_module(String line) throws IOException {
        List<String> tokens = SiderUtil.tokenize(line);
        if (tokens.size() >= 4 && !tokens.get(3).equalsIgnoreCase("from"))
            throw new RuntimeException("Token 'FROM' expected");
        if (tokens.size() < 5)
            throw new RuntimeException("Too few arguments");

        //find specified type
        ModuleGetter getter = ModuleGetter.getForType(tokens.get(4));

        //download and register module (create default module.sider if not existing)
        Module mod = getter.getModule(tokens.get(0), tokens.get(1), tokens.get(2), tokens.get(4));
        if (mod == null) return null; //probably user cancelled

        File md = new File(mod.getModuleDirectory(), "module.sider");
        if (!md.exists()) {
            if (md.isDirectory())
                throw new IOException("Can't create module.sider - is directory");
            BufferedWriter bw = new BufferedWriter(new OutputStreamWriter(new FileOutputStream(md)));
            bw.write(String.format("module %s %s %s%s", mod.getAuthor(), mod.getName(), mod.getVersion(), System.lineSeparator()));
            bw.flush(); //other information from mod not interesting after first run / not available
            bw.close();
        }

        //install module (build with setup script if existing)
        Optional<File> sscr = mod.getSetupScript();
        if (sscr.isPresent()) {
            System.out.println("Performing one-time setup for "+mod.getAuthor()+"-"+mod.getName()+"...");
            new ScriptEngine(sscr.get().getAbsolutePath(), mod.getModuleDirectory().toPath()).run();
        }

        //add to registered modules
        SiderUtil.modules.update();
        return mod;
    }

    /** rerun setup script */
    private Module strategy_build(String line) throws IOException {
        List<String> tokens = SiderUtil.tokenize(line);
        if (tokens.size() < 2)
            throw new RuntimeException("Module author and name expected");

        //Lookup module
        Optional<Module> mod = SiderUtil.modules.findModuleExact(tokens.get(0), tokens.get(1));
        if (!mod.isPresent())
            throw new RuntimeException("Module not installed");

        //try to hot-load the module.sider
        try {
            File siderFile = new File(mod.get().getModuleDirectory(), "module.sider");
            mod = Optional.of(ModuleCrawler.parseModuleDefinition(siderFile));
            SiderUtil.modules.updateSingleModule(mod.get());
        } catch (Exception e) {
            System.err.println(e.getMessage());
        }

        //install module (build with setup script if existing)
        Optional<File> sscr = mod.get().getSetupScript();
        if (sscr.isPresent()) {
            System.out.println("Running setup script for "+mod.get().getAuthor()+"-"+mod.get().getName()+" from "+mod.get().getModuleDirectory().toPath());
            new ScriptEngine(sscr.get().getAbsolutePath(), mod.get().getModuleDirectory().toPath()).run();
        } else
            throw new RuntimeException("No setup script set in module.sider");

        //add to registered modules
        SiderUtil.modules.update();
        System.out.println();
        return mod.get();
    }

    private void strategy_search(String line) throws IOException {
        List<String> tokens = SiderUtil.tokenize(line);
        if (tokens.size()==0) throw new RuntimeException("No search expression");
        else if (tokens.size() == 1) {
            Pattern rex = Pattern.compile(tokens.get(0));
            System.out.println("---===    Modules    ===---");
            SiderUtil.modules.getInstalledModules().stream().filter(m->
                rex.matcher(m.getAuthor()).matches() ||
                rex.matcher(m.getName()).matches()
            ).forEach(m->{
                System.out.printf(" - %s %s [%s]%s", m.getAuthor(), m.getName(), m.getVersion().toString(), System.lineSeparator());
            });
            System.out.println("---===     Tools     ===---");
            SiderUtil.tools.getTools().stream().filter(t->
                    rex.matcher(t).matches()
            ).forEach(m->{
                System.out.printf(" - %s%s", m, System.lineSeparator());
            });
        } else if (tokens.size() > 2) {
            Pattern rexAuthor = Pattern.compile(tokens.get(0));
            Pattern rexName = Pattern.compile(tokens.get(1));
            System.out.println("---===    Modules    ===---");
            SiderUtil.modules.getInstalledModules().stream().filter(m->
                    rexAuthor.matcher(m.getAuthor()).matches() ||
                    rexName.matcher(m.getName()).matches()
            ).forEach(m->{
                System.out.printf(" - %s %s [%s]%s", m.getAuthor(), m.getName(), m.getVersion().toString(), System.lineSeparator());
            });
        }
    }

    /** cleans includes, libs and bin directory before re-indexing them */
    private void strategy_clean() {
        System.out.print("Cleaning up includes...\r");
        SiderUtil.delete_file(SiderUtil.GLOBAL_INCLUDE_DIR.toFile());
        System.out.print("Cleaning up libs...    \r");
        SiderUtil.delete_file(SiderUtil.GLOBAL_LIBS_DIR.toFile());
        System.out.print("Cleaning up bin... \r");
        SiderUtil.delete_file(SiderUtil.GLOBAL_BIN_DIR.toFile());
        System.out.print("Indexing...       \r");

        try { SiderUtil.modules.update(); } catch (Exception e) { e.printStackTrace(); }
        try { SiderUtil.tools.update(); } catch (Exception e) { e.printStackTrace(); }
        System.out.println();
    }

}
