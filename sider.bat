@echo off
rem base script to start sider

rem search java in case it's not on path
echo Test Path
call java.exe -version>>:NULL
if /I %ERRORLEVEL% EQU 0 (
set javabinary=java
goto findjar
)
rem could not execute java -> it's not in path
rem JAVA_HOME seems to be a thing
echo Test JAVA_HOME
rem check if env is set
if NOT DEFINED JAVA_HOME goto nojre
rem check if path contains spaces, quote path if so
if NOT "%JAVA_HOME: =%"=="%JAVA_HOME%" (
  set javabinary=^"%JAVA_HOME%\bin\java.exe^"
 ) else (
  set javabinary=%JAVA_HOME%\bin\java.exe
 )
rem check if java actually exists at the specified path
call %javabinary% -version>>:NULL
if /I %ERRORLEVEL% EQU 0 goto findjar
goto nojre

:findjar
rem search latest build in root dir
for /F "tokens=*" %%x in ('dir /b sider-*.jar') do (
set siderjar=%%x
goto run
)
goto missing

:run
cls
call java -jar %siderjar% %*
goto :eof

:missing
cls
echo Could not locate any build within the current directory!
echo.
echo Please download a Tag from GitLab or build the project before trying to execute
echo this script again.
echo.
pause
goto :eof

:nojre
cls
echo Unable to locate java on your system
echo.
echo It is recommended to add the java to your environment PATH for easy use
goto :eof